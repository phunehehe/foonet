{ pkgs ? import <nixpkgs> {} }:

let

  name = "foonet";

  executable = main: {
    inherit main;
    dependencies = [
      name
    ];
    ghc-prof-options = [
      "-O2"
      "-fprof-auto"
      "-rtsopts"
    ];
    ghc-options = [
      "-threaded"
      "-with-rtsopts=-N"
    ];
    source-dirs = [
      "src"
    ];
  };

in pkgs.callPackage ./nix2cabal {

  spec = {
    inherit name;

    default-extensions = ["OverloadedStrings"];

    dependencies = [
      "aeson"
      "base"
      "bytestring"
      "cond"
      "deepseq"
      "directory"
      "filepath"
      "foldl"
      "hmatrix"
      "lens"
      "mtl"
      "random"
      "random-shuffle"
      "safe"
      "scientific"
      "text"
      "time"
      "unordered-containers"
      "vector"
      "wreq"
    ];

    executables = {
      benchmark-graph = executable "Benchmark/Graph.hs";
      benchmark-learn = executable "Benchmark/Learn.hs";
    };

    library = {
      exposed-modules = [
        "NN.Library"
      ];
      source-dirs = [
        "lib"
      ];
    };

    ghc-options = [
      "-Wall"
    ];

    tests.spec = {
      main = "Spec.hs";
      dependencies = [
        "hspec"
      ];
      source-dirs = [
        "lib"
        "test"
      ];
    };
  };
}
