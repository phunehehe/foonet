$(function () {

    function drawGraph(data) {

        var graph = $('<div></div>');
        $('body').append(graph);

        var series = [];
        for (var s of Object.keys(data).sort()) {
            series = series.concat([{
                name: s,
                data: data[s],
            }]);
        };

        graph.highcharts({
            chart: {
                zoomType: 'x'
            },
            tooltip: {
                crosshairs: true,
                shared: true,
            },
            title: {
                text: 'Benchmark'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            series: series,
        });
    };

    $.getJSON('./graph.json', function (data) {
        drawGraph(data);
    });
});
