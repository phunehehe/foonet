import qualified Numeric.LinearAlgebra.HMatrix as MT

import           Control.Monad.State           (evalState)
import           Data.Foldable                 (foldl')
import           Numeric.LinearAlgebra.HMatrix ((===), (><))
import           System.Random                 (mkStdGen)
import           Test.Hspec                    (describe, hspec, it, shouldBe)

import           NN.Data
import           NN.Internal


singleSample :: [Node] -> [Node] -> NormalizedSample
singleSample inputs outputs = NormalizedSample inputs' outputs'
    where
        inputs' = MT.asRow $ MT.fromList inputs
        outputs' = MT.asRow $ MT.fromList outputs

concatSample :: NormalizedSample -> NormalizedSample -> NormalizedSample
concatSample s1 s2 = NormalizedSample inputs outputs
    where
        inputs = sInputs s1 === sInputs s2
        outputs = sOutputs s1 === sOutputs s2

concatSamples :: [NormalizedSample] -> NormalizedSample
concatSamples [] = NormalizedSample empty empty
    where
        empty = (0><0) []
concatSamples (x:xs) = foldl' concatSample x xs


main :: IO ()
main = hspec $ do

    describe "NN.Internal" $ do
        let

            learningRate = 1e-6

            dimensions = [9, 3, 3, 1]
            sample = concatSamples $ replicate 100 $ singleSample (take 9 [1..]) [0]
            weights = evalState (randomWeights dimensions) (mkStdGen 1)
            nodes = forward weights (sInputs sample)
            deltas = backward weights sample nodes

            --simpleDimensions = [2, 2, 2, 1]
            simpleSample = singleSample [2, 3] [simpleExampleOutput]
            simpleWeights :: Weights
            simpleWeights =
                [ MT.fromLists
                    [ [1, 2]
                    , [2, 3]
                    , [3, 4]
                    ]
                , MT.fromLists
                    [ [3, 4]
                    , [4, 5]
                    , [5, 6]
                    ]
                , MT.fromLists
                    [ [5]
                    , [6]
                    , [7]
                    ]
                ]
            simpleNodes = forward simpleWeights (sInputs simpleSample)
            simpleDeltas = backward simpleWeights simpleSample simpleNodes

            simpleExampleOutput = 1 :: Node
            simpleFinalDelta = 2 * (simpleFinalValue - simpleExampleOutput)

            -- Hand calculated, must be correct ;)
            simpleFinalValue = 2317
            simpleFinalValues = MT.fromLists [[simpleFinalValue]]
            simpleFirstDeltas = MT.fromLists [[273288], [333504]]

        describe "randomWeights" $
            it "gives a set of weights for each output layer" $
                countDimensions weights `shouldBe` dimensions

        describe "forward" $ do
            it "gives a set of nodes for each example" $
                length nodes `shouldBe` length dimensions
            it "calculates the final node correctly" $
                predictNormalized simpleWeights (sInputs simpleSample) `shouldBe` simpleFinalValues

        describe "backward" $ do
            it "gives a set of delta nodes for each output layer" $
                (MT.rows <$> deltas) `shouldBe` (MT.cols <$> drop 1 nodes)
            it "calculates the last delta correctly" $
                last simpleDeltas `shouldBe` MT.fromLists [[simpleFinalDelta]]
            it "calculates the first deltas correctly" $
                head simpleDeltas `shouldBe` simpleFirstDeltas

        describe "updateWeights" $
            it "gives a weight for each existing weight" $ do
                let newWeights = updateWeights learningRate weights nodes deltas
                countDimensions newWeights `shouldBe` dimensions

        describe "learn" $
            it "gives a better set of weights" $ do
                let newWeights = learn learningRate weights sample
                score newWeights sample < score weights sample `shouldBe` True
