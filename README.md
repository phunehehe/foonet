# fooNet

## Elevator Pitch

Given the price of a stock over the past 18 months, predict its price in 6
months!

  - Take an unknown function `f` (price of the stock over time)
  - Generate some data points, each with 18 inputs (prices of the stock in each
    month over 18 months) and 1 output (price of the stock 24 months from the
    start)
  - Feed the data to a neural network and train it using backpropagation
  - See what the neural network say about the price of the stock in 6 months
  - Take a long or short position accordingly
  - Profit!

## More Details

This is a toy obviously, caveat emptor :P

More seriously though, this works rather well, at least technically.

  - Have a look at `src/Benchmark/Common.hs` to see now the neural network is
    configured.
  - Run `cabal run benchmark-learn` to train it
  - Run `cabal run benchmark-graph` to generate a graph showing how well it
    performs. Run e.g. `twistd --nodaemon web --path .` to serve the graph,
    view it at http://localhost:8080/benchmark/.

After a few minutes of training the graph should look like this:

![graph.png](/graph.png)

## Development Notes

  - If you use [Nix](https://nixos.org/nix/), `nix-shell --run 'cabal configure --enable-tests'`
    should have you covered
  - Otherwise find the Cabal file as an
    [artifact](https://gitlab.com/phunehehe/foonet/builds/artifacts/master/browse?job=test)
    (read more about this at [nix2cabal](https://gitlab.com/phunehehe/nix2cabal))
