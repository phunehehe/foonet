{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}

module NN.Data where

import qualified Numeric.LinearAlgebra.HMatrix as MT

import           Control.Lens                  (makeLenses)
import           Data.Aeson                    (FromJSON, ToJSON)
import           GHC.Generics                  (Generic)
import           Numeric.LinearAlgebra.HMatrix (Matrix, Vector)

import           NN.Orphans                    ()


type Dimension = Integer

type Deltas = [Matrix Delta]
type Nodes = [Matrix Node]
type Weights = [Matrix Weight]

type Delta = Double
type ErrorRate = Double
type LearnRate = Double
type Node = Double
type Weight = Double


data Example = Example
    { reInputs  :: Vector Node
    , reOutputs :: Vector Node
    }
example :: [Node] -> [Node] -> Example
example inputs outputs = Example (MT.fromList inputs) (MT.fromList outputs)

data Sample = Sample
    { rsInputs  :: Matrix Node
    , rsOutputs :: Matrix Node
    } deriving Show

data NormalizedExample = NormalizedExample
    { eInputs  :: Vector Node
    , eOutputs :: Vector Node
    }

data NormalizedSample = NormalizedSample
    { sInputs  :: Matrix Node
    , sOutputs :: Matrix Node
    }

data Network = Network
    { nWeights :: Weights
    , nError   :: ErrorRate
    } deriving Generic
instance FromJSON Network
instance ToJSON Network

data MyState = MyState
    { _msWarmSample :: NormalizedSample
    , _msWeights    :: Weights
    }

makeLenses ''MyState
