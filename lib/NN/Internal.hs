{-# LANGUAGE BangPatterns          #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE RecordWildCards       #-}

module NN.Internal where

import qualified Control.Monad.State           as S
import qualified Data.Aeson                    as A
import qualified Data.ByteString               as B
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Char8    as C
import qualified Numeric.LinearAlgebra.HMatrix as MT

import           Control.Concurrent            (MVar, forkIO, modifyMVar_,
                                                newEmptyMVar, putMVar, readMVar,
                                                threadDelay)
import           Control.DeepSeq               (force)
import           Control.Exception             (tryJust)
import           Control.Lens                  ((&), (.~))
import           Control.Monad                 (ap, forever, guard, liftM2)
import           Control.Monad.State           (State)
import           Data.Aeson                    (ToJSON)
import           Data.Foldable                 (foldl', toList)
import           Data.List                     (genericReplicate, genericTake)
import           Data.Maybe                    (fromMaybe)
import           Numeric.LinearAlgebra.Devel   (foldVector)
import           Numeric.LinearAlgebra.HMatrix (Container, Matrix, Vector, (<>),
                                                (|||))
import           System.Directory              (createDirectoryIfMissing,
                                                renameFile)
import           System.FilePath               (dropFileName)
import           System.IO.Error               (isDoesNotExistError)
import           System.Random                 (StdGen, newStdGen, randomR)
import           System.Random.Shuffle         (shuffleM)
import           Text.Printf                   (printf)

import           NN.Data


-- Each input is a row, each output is a column
randomLayer :: Dimension -> Dimension -> State StdGen (Matrix Weight)
randomLayer inputs outputs = do
    weights <- genericReplicateM (outputs * inputs') randomSt
    return $ MT.matrix (fromIntegral outputs) weights
    where
        -- One more for the threshold
        inputs' = inputs + 1
        randomSt = S.state $ randomR (-1, 1)

randomWeights :: [Dimension] -> State StdGen Weights
randomWeights (i:o:xs) = do
    weights <- randomLayer i o
    nextWeights <- randomWeights (o:xs)
    return $ weights : nextWeights
randomWeights [_] = return []
randomWeights [] = error "randomWeights got empty dimensions"


forward' :: Nodes -> Matrix Weight -> Nodes
forward' knownNodes weights = knownNodes ++ myNodes
    where
        myNodes = [myInputs <> weights]
        -- Add threshold
        myInputs = 1 ||| last knownNodes

forward :: Weights -> Matrix Node -> Nodes
forward weights inputs = foldl' forward' [inputs] weights


backward' :: Deltas -> Matrix Weight -> Deltas
backward' deltas@(nextDeltas:_) weights = myDeltas : deltas
    where
        myDeltas = MT.dropRows 1 weights <> nextDeltas
backward' [] _ = error "backward' got empty deltas"

-- We aren't using a nonlinearity (yet?), so the signal is the same as the
-- final value.
backward :: Weights -> NormalizedSample -> Nodes -> Deltas
backward weights NormalizedSample{..} nodes =
    foldl' backward' [MT.tr firstDeltas] . reverse $ tail weights
    where
        firstDeltas = 2 * (last nodes - sOutputs)


getWeightUpdates :: LearnRate -> Matrix Node -> Matrix Delta -> Matrix Double
getWeightUpdates rate nodes deltas = MT.scalar rate * updates
    where
        updates = sum $ zipWith MT.outer (MT.toRows nodes') (MT.toColumns deltas)
        -- Add threshold
        nodes' = 1 ||| nodes

updateWeights :: LearnRate -> Weights -> Nodes -> Deltas -> Weights
updateWeights rate weights nodes deltas = newWeights
    where
        newWeights = zipWith (-) weights updates
        updates = zipWith (getWeightUpdates rate) nodes deltas


learn :: LearnRate -> Weights -> NormalizedSample -> Weights
learn rate weights sample@NormalizedSample{..} = newWeights
    where
        nodes = forward weights sInputs
        deltas = backward weights sample nodes
        newWeights = updateWeights rate weights nodes deltas


predictNormalized :: Weights -> Matrix Node -> Matrix Node
predictNormalized = (last .) . forward

predict :: Network -> [Node] -> [Node]
predict Network{..} inputs =
    MT.toList . MT.flatten . denormalizeMatrix inputs' .
        predictNormalized nWeights $ normalizeMatrix inputs'
    where
        inputs' = MT.asRow $ MT.fromList inputs


toRawExamples :: Sample -> [Example]
toRawExamples Sample{..} = zipWith Example (MT.toRows rsInputs) (MT.toRows rsOutputs)

toSample :: [NormalizedExample] -> NormalizedSample
toSample = ap (NormalizedSample . MT.fromRows . fmap eInputs) (MT.fromRows . fmap eOutputs)


getRange :: (Container c e) => c e -> (e, e)
getRange = liftM2 (,) MT.minElement MT.maxElement

normalize :: (Node, Node) -> Node -> Node
normalize (minValue, maxValue) node = (node - minValue) / (maxValue - minValue)

denormalize :: (Node, Node) -> Node -> Node
denormalize (minValue, maxValue) node = node * (maxValue - minValue) + minValue

normalizeVector :: Vector Node -> Vector Node
normalizeVector = MT.cmap =<< normalize . getRange

denormalizeVector :: Vector Node -> Vector Node -> Vector Node
denormalizeVector = MT.cmap . denormalize . getRange

normalizeMatrix :: Matrix Node -> Matrix Node
normalizeMatrix = MT.fromRows . fmap normalizeVector . MT.toRows

denormalizeMatrix :: Matrix Node -> Matrix Node -> Matrix Node
denormalizeMatrix = (MT.fromRows .) . (. MT.toRows) . zipWith denormalizeVector . MT.toRows

normalizeSample :: Sample -> NormalizedSample
normalizeSample = toSample . fmap normalizeExample . toRawExamples

normalizeExample :: Example -> NormalizedExample
normalizeExample Example{..} = NormalizedExample inputs outputs
    where
        inputs = normalizeVector reInputs
        outputs = MT.cmap (normalize (getRange reInputs)) reOutputs


vectorMean :: Vector Double -> Double
vectorMean xs = s / l
    where
        (s, l) = foldVector f (0, 0) xs
        f x (s', l') = (s' + x, l' + 1)

score :: Weights -> NormalizedSample -> ErrorRate
score weights NormalizedSample{..} = vectorMean . MT.flatten . MT.cmap abs $ predictions - sOutputs
    where
        predictions = predictNormalized weights sInputs

countWeights :: Weights -> Integer
countWeights = foldl' f 0
    where
        f z m = z + fromIntegral (MT.rows m) * fromIntegral (MT.cols m)

countDimensions :: Weights -> [Dimension]
countDimensions weights@(firstWeights : _) = inputs : outputs
    where
        inputs = fromIntegral (MT.rows firstWeights - 1)
        outputs = (fromIntegral . MT.cols) <$> toList weights
countDimensions [] = []

loadWeights :: FilePath -> [Dimension] -> IO Weights
loadWeights saveFile dimensions = do
    maybeNetwork <- tryJust notExist $ loadNetwork saveFile
    case maybeNetwork of
        Left _ -> do
            stdGen <- newStdGen
            return $ S.evalState (randomWeights dimensions) stdGen
        Right Network{nWeights} -> do
            let actualDimensions = countDimensions nWeights
            if actualDimensions == dimensions
            then return nWeights
            else error' ("network does not have expected dimensions" :: String,
                saveFile, dimensions, actualDimensions)
    where
        notExist = guard . isDoesNotExistError

loadNetwork :: FilePath -> IO Network
loadNetwork saveFile = (fromMaybe panic . A.decodeStrict) <$> contents
    where
        contents = B.readFile saveFile
        panic = error' ("cannot decode file" :: String, saveFile)


randomFromSample :: Integer -> [NormalizedExample] -> IO NormalizedSample
randomFromSample n examples =
    (concatExamples . genericTake n) <$> shuffleM examples


isValid :: Double -> Bool
isValid = not . liftM2 (||) isNaN isInfinite


report :: FilePath -> [NormalizedExample] -> NormalizedSample -> Integer -> MVar MyState -> IO ()
report path trainSample testSample batchSize state = do

    threadDelay $ 10 ^ (7 :: Integer)

    roState <- readMVar state
    savedNetwork <- loadNetwork path

    let
        errIn = score weights (_msWarmSample roState)
        errOut = score weights testSample
        savedError = nError savedNetwork
        swap = path ++ ".swap"
        weights = _msWeights roState

    if errOut < savedError
        then do
            createDirectoryIfMissing True $ dropFileName path
            -- Somewhat atomic saving
            BL.writeFile swap . A.encode $ Network weights errOut
            renameFile swap path
            printf "In sample error    : %f\n" errIn
            printf "Out of sample error: %f\n" errOut
        else modifyMVar_ state $ \myState -> do
            printf "Out of sample error: %f, reverting to %f\n" errOut savedError
            return $ MyState (_msWarmSample myState) (nWeights savedNetwork)

    modifyMVar_ state $ \myState -> do
        warmSample' <- randomFromSample batchSize trainSample
        return $ MyState warmSample' (_msWeights myState)


concatExamples :: [NormalizedExample] -> NormalizedSample
concatExamples examples = NormalizedSample inputs outputs
    where
        inputs = MT.fromRows $ eInputs <$> examples
        outputs = MT.fromRows $ eOutputs <$> examples


concatRawExamples :: [Example] -> Sample
concatRawExamples examples = Sample inputs outputs
    where
        inputs = MT.fromRows $ reInputs <$> examples
        outputs = MT.fromRows $ reOutputs <$> examples


learnWithState :: LearnRate -> MVar MyState -> IO ()
learnWithState rate state = modifyMVar_ state $ \myState@MyState{..} ->
    let !newWeights = force $ learn rate _msWeights _msWarmSample
    in return $ myState & msWeights .~ newWeights

splitSample :: NormalizedSample -> [NormalizedExample]
splitSample NormalizedSample{..} = zipWith NormalizedExample (MT.toRows sInputs) (MT.toRows sOutputs)

learnReport :: FilePath -> [Dimension] -> LearnRate -> [Example] -> [Example] -> Integer -> IO ()
learnReport saveFile dimensions rate trainExamples testExamples batchSize = do
    weights <- loadWeights saveFile dimensions
    state <- newEmptyMVar
    let
        trainSample = concatRawExamples trainExamples
        testSample = concatRawExamples testExamples
        trainSample' = splitSample $ normalizeSample trainSample
        testSample' = normalizeSample testSample
    warmSample <- randomFromSample batchSize trainSample'
    putMVar state $ MyState warmSample weights
    _ <- forkIO . forever $ report saveFile trainSample' testSample' batchSize state
    forever $ learnWithState rate state


error' :: ToJSON a => a -> t
error' = error . C.unpack . A.encode

genericReplicateM :: (Integral i, Monad m) => i -> m a -> m [a]
genericReplicateM n x = sequence (genericReplicate n x)
