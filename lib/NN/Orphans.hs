{-# OPTIONS_GHC -fno-warn-orphans #-}

module NN.Orphans where

import qualified Data.Aeson                    as A
import qualified Numeric.LinearAlgebra.HMatrix as M

import           Data.Aeson                    (FromJSON, ToJSON)
import           Numeric.LinearAlgebra.HMatrix (Element, Matrix)

instance (FromJSON a, Element a) => FromJSON (Matrix a) where
    parseJSON = fmap M.fromLists . A.parseJSON
instance (ToJSON a, Element a) => ToJSON (Matrix a) where
    toJSON = A.toJSON . M.toLists
