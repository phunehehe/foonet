import qualified Data.Aeson           as A
import qualified Data.ByteString.Lazy as B
import qualified Data.HashMap.Lazy    as HM

import           Data.HashMap.Lazy    (HashMap)
import           Data.List            (sort)
import           System.FilePath      ((</>))
import           System.Random        (newStdGen)

import           Benchmark.Common
import           NN.Library


makePredictions :: Network -> [Double] -> [(Double, Double)]
makePredictions network startXs = zip outputXs predictions
    where
        inputs = toInputs <$> startXs
        outputXs = outputX <$> startXs
        predictions = predict network =<< inputs


toSeries :: [Double] -> Network -> HashMap String [(Double, Double)]
toSeries startXs network = HM.fromList
    [ ("Benchmark", f' <$> startXs)
    , ("Predictions", makePredictions network startXs)
    ]
   where
       f' x = (x, f x)


main :: IO ()
main = do
    stdGen <- newStdGen
    network <- loadNetwork networkFile
    let
        startXs = sort $ randomList 1000 stdGen
    B.writeFile (outDir </> "graph.json") $ A.encode $ toSeries startXs network
