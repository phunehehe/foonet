import           Data.List        (genericLength)
import           System.Random    (newStdGen)

import           Benchmark.Common
import           NN.Library


main :: IO ()
main = do
    stdGen <- newStdGen
    let
        trainExamples = toExample <$> randomList 100 stdGen
        testExamples = toExample <$> randomList 100 stdGen
    learnReport networkFile dimensions 1e-6 trainExamples testExamples
        (genericLength trainExamples)
