module Benchmark.Common where

import           Data.List       (genericTake, unfoldr)
import           System.FilePath ((</>))
import           System.Random   (StdGen, randomR)

import           NN.Library


f :: Double -> Double
f x = (x - 2) * (x + 3) + sin x * 100

stepSize :: Double
stepSize = 0.1

numberOfInputs :: Integer
numberOfInputs = 18

outputOffset :: Integer
outputOffset = 24

dimensions :: [Dimension]
dimensions = [18, 9, 1]

outDir :: FilePath
outDir = "benchmark"

networkFile :: FilePath
networkFile = outDir </> "network.json"

randomList :: Integer -> StdGen -> [Double]
randomList n = genericTake n . unfoldr (Just . randomR (-10, 10))

toInputs :: Double -> [Double]
toInputs startX = f . xAt startX <$> [0 .. (numberOfInputs - 1)]

xAt :: Double -> Integer -> Double
xAt startX step = startX + stepSize * fromIntegral step

outputX :: Double -> Double
outputX startX = xAt startX outputOffset

toExample :: Double -> Example
toExample startX = example inputs [output]
    where
        inputs = toInputs startX
        output = f $ outputX startX
